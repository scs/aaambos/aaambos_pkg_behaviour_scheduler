from __future__ import annotations

from abc import abstractmethod
from datetime import datetime
from typing import Any, TYPE_CHECKING, Callable

from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.topic import Topic
from aaambos.std.extensions.message_cache.message_interest import MessageInterest
from aaambos.std.extensions.restricted_resouce.applicant import ResourceApplicantInterface, ResourceIdentifier

if TYPE_CHECKING:
    from behaviour_scheduler.modules.behaviour_scheduler import BehaviourScheduler, BehaviourSchedulerConfig


class Behaviour(ResourceApplicantInterface):
    """Abstract base class for implementing behaviours.

    You need to provide the promises that can result in the installation of the behaviour class when received (get_controlling_msg_promises).

    Such msg is check in is_initialising_controlling_msg, if it really is instantiating the behaviour.

    Interests in other topics / messages is done via "MessageInterests". They are defined in 'get_msg_interests'

    "get_resource_names" defines what (restricted) resources the behaviour uses / will (maybe) want to acquire later.
    """

    def __init__(self, scheduler, topic: Topic, msg, log):
        self.scheduler: BehaviourScheduler = scheduler
        """The module the behaviour is running in (the `behaviour_scheduler.modules.behaviour_scheduler.BehaviourScheduler`)."""
        self.initialising_controlling_msg: tuple[Topic, Any] = topic, msg
        """The msg that created/initialised the behaviour (topic and msg)."""
        self.active: bool = True
        """Turn to False when the behavior terminates."""
        self.acquired_resources: set[ResourceIdentifier] = set()
        """Set of current acquired resources."""
        self.name: str = f"{self.__class__.name}_{datetime.now().isoformat()}"
        """Used to identify the behaviour."""
        self._registered_callbacks: list[tuple[CommunicationPromise, Callable]] = []
        """Stores registered callbacks, used for cleanup"""
        self.log = log
        """Logger"""

    @classmethod
    @abstractmethod
    def get_controlling_msg_promises(cls, module_config: BehaviourSchedulerConfig) -> list[CommunicationPromise]:
        """Provide the promises that can start/activate the behaviour.

        Args:
            module_config: if the configuration changes the start/activation

        Returns:
            list of communication promises that can trigger an activation of the behaviour.
        """
        ...

    @classmethod
    @abstractmethod
    def get_msg_interests_wo_callbacks(cls, module_config: BehaviourSchedulerConfig) -> list[MessageInterest]:
        """The MessageInterests (including the promises) that the behaviour is interested in.

        Because of the cls, callbacks on an instance is not possible. Therefore, ignore them.
        Args:
            module_config: for configuration purposes.

        Returns:
            the list of message interests with defined promises and callbacks of the behaviour
        """
        ...

    @abstractmethod
    def get_msg_interests(self, module_config : BehaviourSchedulerConfig) -> list[MessageInterest]:
        """Same as `get_msg_interests_wo_callbacks` but now with callbacks.

        Args:
            module_config: for configuration purposes

        Returns:
            the list of message interests with defined promises and callbacks of the behaviour
        """
        ...

    @staticmethod
    def starts_with_module(module_config: BehaviourSchedulerConfig) -> bool:
        """If the behaviour is initialized with the behaviour scheduler. Then topic and msg in __init__ are None."""
        return False

    @classmethod
    @abstractmethod
    def is_initialising_controlling_msg(cls, topic: Topic, msg, active_behaviours: list[Behaviour]) -> bool:
        """Check if the controlling message does indeed initialise the behaviour."""
        ...

    @abstractmethod
    async def initialize(self):
        ...

    @abstractmethod
    async def step(self):
        ...

    @classmethod
    @abstractmethod
    def get_resource_names(cls, module_config: BehaviourSchedulerConfig) -> list[ResourceIdentifier]:
        """Provide the used resources by the behaviour.

        Args:
            module_config: for configuration purposes.

        Returns:
            list of resource identifiers that the behaviour will probably request to acquire.
        """
        ...

    async def handle_resource_loss(self, resource_name: ResourceIdentifier):
        """Is called when the resource is acquired from a behaviour with a higher priority (after the resource was acquired).

        Args:
            resource_name: the identifier of the resource that the behaviour lost.
        """
        if resource_name in self.acquired_resources:
            self.acquired_resources.remove(resource_name)

    async def handle_acquired_resource(self, resource_name: ResourceIdentifier):
        """Is called when a requested resource is acquired. Overwrite based on your needs.

        Args:
            resource_name: the identifier of the acquired resource.
        """
        self.acquired_resources.update([resource_name])

    def is_active(self):
        """If this returns False the behaviour is processed as finished.

        Returns:
            the boolean value if the step function should be called and if the cleanup method is called.
        """
        return self.active

    async def send(self, topic: Topic, msg: Any):
        """Wrapper for sending a message via a topic.

        Args:
            topic: the topic of the message.
            msg: the message payload
        """
        await self.scheduler.com.send(topic, msg)

    async def register_callback_for_promise(self, promise: CommunicationPromise, callback: Callable):
        """Wrapper for registering callbacks with promise but storing them for later cleanup.

        Args:
            promise: the promise with information about topic and payload wrapper.
            callback: the callback to use. (async function)
        """
        self._registered_callbacks.append((promise, callback))
        await self.scheduler.com.register_callback_for_promise(promise, callback)

    async def unregister_callback(self, topic: Topic, callback: Callable):
        """Wrapper for unregister callbacks.

        Args:
            topic: the topic to unregister. (promise.settings.topic)
            callback: the callback method to unregister.
        """
        idx_list = [i for i, (promise, c) in enumerate(self._registered_callbacks) if promise.settings.topic == topic and callback == c]
        if idx_list:
            self._registered_callbacks.pop(idx_list[0])
        await self.scheduler.com.unregister_callback(topic, callback)

    @classmethod
    async def register_all_msg_interests_wo_callback(cls, scheduler, module_config: BehaviourSchedulerConfig):
        """Register the callbacks for all msg interests defined in `get_msg_interests_wo_callbacks`.

        Is called by the behaviour scheduler during initialization of the scheduler. Overwrite, if you do not want to register all msg interests.

        Used for caching the msgs not the callbacks
        Args:
            module_config: for configuration purposes.
        """
        await scheduler.msg_cache.register_interest(cls.get_msg_interests_wo_callbacks(module_config))

    async def register_all_msg_interests(self, module_config: BehaviourSchedulerConfig):
        """Register the callbacks for all msg interests defined in `get_msg_interests`.

        Is called by the behaviour scheduler before initialize. Overwrite, if you do not want to register all msg interests.

        Args:
            module_config: for configuration purposes.
        """
        await self.scheduler.msg_cache.register_interest(self.get_msg_interests(module_config))

    async def unregister_all_msg_interests(self, module_config: BehaviourSchedulerConfig):
        """Unregister all callbacks for all msg interests.

        Is called in the cleanup method.

        Args:
            module_config:
        """
        await self.scheduler.msg_cache.unregister_interest(self.get_msg_interests(module_config))

    async def clean_up(self):
        """Clean up method that releases acquired restricted resources and unregisters callbacks."""
        for promise, callback in self._registered_callbacks:
            await self.scheduler.com.unregister_callback(promise.settings.topic, callback)
        await self.scheduler.resources.clean_applicant(self)
        await self.unregister_all_msg_interests(self.scheduler.config)
