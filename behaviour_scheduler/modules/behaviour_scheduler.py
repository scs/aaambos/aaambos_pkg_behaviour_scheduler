from __future__ import annotations

from collections import defaultdict
from distutils.extension import Extension
from typing import Type, Any

from aaambos.core.communication.service import CommunicationService
from aaambos.core.communication.topic import TopicId, Topic
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from aaambos.std.extensions.message_cache.message_cache import MessageCacheExtensionFeature, MessageCacheExtension
from aaambos.std.extensions.restricted_resouce.resticted_resource import RestrictedResourceExtensionFeature, \
    RestrictedResourceExtension
from attrs import define, field

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity

from behaviour_scheduler.modules.behaviour import Behaviour


class SharedMemory:
    """Just store stuff in the behaviour scheduler, so that other behaviours can access them.

    Instead of a dict. You can just write to it like a normal class. E.g.,

    ```python
    # behaviour 1
    self.scheduler.shared_memory.my_var = 123

    # behaviour 2
    my_var = self.scheduler.shared_memory.my_var
    ```
    """
    ...


class BehaviourScheduler(Module, ModuleInfo):
    config: BehaviourSchedulerConfig
    com: CommunicationService | MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute

    def __init__(self, config: BehaviourSchedulerConfig, com, log, ext: dict[str, Extension], msg_cache: MessageCacheExtension, resources: RestrictedResourceExtension, *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.msg_cache = msg_cache
        self.resources = resources
        self.shared_memory = SharedMemory()
        self.available_behaviours = self.config.behaviours
        self.control_topic_to_behaviour: dict[TopicId, list[Type[Behaviour]]] = defaultdict(list)
        self.active_behaviours: list[Behaviour] = []

    @classmethod
    def provides_features(cls, config: BehaviourSchedulerConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        base = {}
        base.update(config.get_provided_features_from_behaviours())
        return base

    @classmethod
    def requires_features(cls, config: BehaviourSchedulerConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        base = {
            RestrictedResourceExtensionFeature.name: (RestrictedResourceExtensionFeature, SimpleFeatureNecessity.Required),
            MessageCacheExtensionFeature.name: (MessageCacheExtensionFeature, SimpleFeatureNecessity.Required)
        }
        base.update(config.get_required_features_from_behaviours())
        return base

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return BehaviourSchedulerConfig

    async def initialize(self):
        # setup msg interests
        # setup initializing controlling topics
        # setup resources
        # setup behaviours that start up with module
        known_topics = set()
        self.log.trace("Setup available behaviours.")
        for behaviour_class in self.available_behaviours:
            self.resources.create_resource(behaviour_class.get_resource_names(self.config))
            await self.msg_cache.register_interest(behaviour_class.get_msg_interests_wo_callbacks(self.config))
            for promise in behaviour_class.get_controlling_msg_promises(self.config):
                self.control_topic_to_behaviour[promise.settings.topic.id()].append(behaviour_class)
                if promise.settings.topic.id() not in known_topics:
                    await self.com.register_callback_for_promise(promise, self.handle_controlling_msg)
                    known_topics.update([promise.settings.topic.id()])

        self.log.info("Setup behaviours that start with module.")
        for behaviour_class in self.available_behaviours:
            if behaviour_class.starts_with_module(self.config):
                await self.start_behaviour(behaviour_class, None, None)

    async def step(self):
        # check if behaviour is alive
        # do step
        # check if behaviour is alive, cleanup
        inactive_behaviors = []
        for idx, behaviour in enumerate(self.active_behaviours):
            await behaviour.step()
            if not behaviour.is_active():
                self.log.info(f"{behaviour.name} is inactive. Clean up.")
                inactive_behaviors.append(idx)
                await behaviour.clean_up()
        for inactive_idx in reversed(inactive_behaviors):
            self.active_behaviours.pop(inactive_idx)

    async def handle_controlling_msg(self, topic: Topic, msg):
        """Check if the controlling msg starts a behaviour then start it. Callback method for controlling msgs.

        Args:
            topic: the topic of the controlling msg.
            msg: the controlling msg content/payload.
        """
        # call all behavior that uses this topic as controlling
        # if it is initializing init this behavior
        for behaviour_class in self.control_topic_to_behaviour[topic.id()]:
            if behaviour_class.is_initialising_controlling_msg(topic, msg, self.active_behaviours):
                await self.start_behaviour(behaviour_class, topic, msg)

    async def start_behaviour(self, behaviour_class, topic, msg):
        """Start the behaviour and add it to the active behaviours.

        Args:
            behaviour_class: the class to instantiate.
            topic: the topic of the initializing/controlling msg.
            msg: the content of the controlling msg.
        """
        self.log.info(f"Start behaviour {behaviour_class.__name__!r}")
        behaviour = behaviour_class(self, topic, msg, self.log)
        await behaviour.register_all_msg_interests(self.config)
        self.active_behaviours.append(behaviour)
        await behaviour.initialize()

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class BehaviourSchedulerConfig(ModuleConfig):
    behaviours: list[Type[Behaviour]]
    """list of behaviour classes that are available."""
    behaviour_configuration: dict[str, Any] = field(default=defaultdict())
    """add here configuration parts for the behaviours."""
    module_path: str = "behaviour_scheduler.modules.behaviour_scheduler"
    module_info: Type[ModuleInfo] = BehaviourScheduler
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0

    def get_provided_features_from_behaviours(self) -> dict[str, tuple[Feature, FeatureNecessity]]:
        features = {}
        for behaviour in self.behaviours:
            for msg_interests in behaviour.get_msg_interests_wo_callbacks(self):
                features.update({feature.name: (feature, SimpleFeatureNecessity.Required) for feature in msg_interests.provided_features})
        return features

    def get_required_features_from_behaviours(self) -> dict[str, tuple[Feature, FeatureNecessity]]:
        features = {}
        for behaviour in self.behaviours:
            for msg_interests in behaviour.get_msg_interests_wo_callbacks(self):
                features.update({feature.name: (feature, SimpleFeatureNecessity.Required) for feature in
                                 msg_interests.required_features})
        return features


def provide_module():
    return BehaviourScheduler
