"""
## Behaviour Scheduler

The Behaviour Scheduler handles defined Behaviours which are passed through the config.

In theory, you should not need to adapt the Behaviour Scheduler. You just need to define your Behaviours and pass them
as references/classes in the config.

The Behaviours need to define some relevant classes, which is more elaborated in the behaviour python file.

"""