"""
## Simple Example of Behaviour

Here a simple example of two behaviours is implemented. First a pong behaviour that does the same thing as the Pong Module in the aaambos std package.

Further, it shows how restricted resources are used. They can be requested by different behaviours and are acquired by the one with the highest priority.

Check the implementation of `behaviour_scheduler.configs.example_behaviour.PongBehaviour` and `behaviour_scheduler.configs.example_behaviour.DummyBehaviour`.
"""