from __future__ import annotations

from typing import Any

from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.topic import Topic
from aaambos.std.extensions.message_cache.message_interest import MessageInterest
from aaambos.std.extensions.restricted_resouce.applicant import ResourceIdentifier
from aaambos.std.modules.ping_pong_example import CountingPromise, CountingFeature, CountingMsg

from behaviour_scheduler.modules.behaviour import Behaviour
from behaviour_scheduler.modules.behaviour_scheduler import BehaviourSchedulerConfig


TEST_RESOURCE = "TestResource"


class PongBehaviour(Behaviour):
    """A Behaviour that models the Pong part of the PingPong module from the std library. Further it requests a restricted resource.

    A behaviour can also look up/get the cached message via self.scheduler.msg_cache.get_last_msg(interest_name="CountingMsgInterest").

    Request a resource:
    ```python
    await self.scheduler.resources.request_resource(TEST_RESOURCE, priority=2, applicant=self)
    ```
    """
    counter: int = 0
    name = "PongBehaviour"
    msg : Any = None

    @classmethod
    def get_controlling_msg_promises(cls, module_config: BehaviourSchedulerConfig) -> list[CommunicationPromise]:
        return [CountingPromise]

    @classmethod
    def get_msg_interests_wo_callbacks(cls, module_config: BehaviourSchedulerConfig) -> list[MessageInterest]:
        counting_msg_interest = MessageInterest("CountingMsgInterest", CountingPromise, required_features=[CountingFeature], provided_features=[CountingFeature], callback=None)
        return [counting_msg_interest]

    def get_msg_interests(self, module_config: BehaviourSchedulerConfig) -> list[MessageInterest]:
        counting_msg_interest = MessageInterest("CountingMsgInterest", CountingPromise,
                                                required_features=[CountingFeature],
                                                provided_features=[CountingFeature], callback=self.handle_increased)
        return [counting_msg_interest]

    @classmethod
    def is_initialising_controlling_msg(cls, topic: Topic, msg, active_behaviours: list[Behaviour]) -> bool:
        """Start the behaviour if there is no PongBehaviour already."""
        return not any([b for b in active_behaviours if isinstance(b, PongBehaviour)])

    async def initialize(self):
        await self.handle_increased(*self.initialising_controlling_msg)

    async def step(self):
        pass

    async def handle_increased(self, topic: Topic, msg: CountingMsg):
        if msg.last_module != self.name:
            self.msg = msg
            await self.scheduler.resources.request_resource(TEST_RESOURCE, priority=2, applicant=self)

    @classmethod
    def get_resource_names(cls, module_config: BehaviourSchedulerConfig) -> list[ResourceIdentifier]:
        return [TEST_RESOURCE]

    async def handle_resource_loss(self, resource_name: ResourceIdentifier):
        await super().handle_resource_loss(resource_name)

    async def handle_acquired_resource(self, resource_name: ResourceIdentifier):
        # if self.counter == 0:
        #     await self.status.set_module_status("received and continue counting", {"color": "green"})
        self.counter = self.msg.value
        self.counter += 1
        # print(f"{self.name}: {self.counter}")
        # await asyncio.sleep(WAIT_UNTIL_SEND)
        await self.send(CountingPromise.settings.topic, CountingMsg(self.counter, self.name))
        # == await self.com.send(self.tpc[Counting], self.wpr[Counting](self.counter, self.name))
        await self.scheduler.resources.release_resource(TEST_RESOURCE, self)


class DummyBehaviour(Behaviour):
    """Just a dummy behaviour that requests the `TEST_RESOURCE` with priority 1 and requests it again if it looses it.

    It set itself to inactive after some steps.
    """
    name = "DummyBehaviour"
    num_steps: int = 0

    @classmethod
    def get_controlling_msg_promises(cls, module_config: BehaviourSchedulerConfig) -> list[CommunicationPromise]:
        return []

    @classmethod
    def get_msg_interests_wo_callbacks(cls, module_config: BehaviourSchedulerConfig) -> list[MessageInterest]:
        return []

    def get_msg_interests(self, module_config: BehaviourSchedulerConfig) -> list[MessageInterest]:
        return []

    @classmethod
    def is_initialising_controlling_msg(cls, topic: Topic, msg, active_behaviours: list[Behaviour]) -> bool:
        return  False

    async def initialize(self):
        await self.scheduler.resources.request_resource(TEST_RESOURCE, priority=1, applicant=self)

    async def step(self):
        self.num_steps += 1

    @staticmethod
    def starts_with_module(module_config: BehaviourSchedulerConfig) -> bool:
        """If the behaviour is initialized with the behaviour scheduler. Then topic and msg in __init__ are None."""
        return True

    @classmethod
    def get_resource_names(cls, module_config: BehaviourSchedulerConfig) -> list[ResourceIdentifier]:
        return [TEST_RESOURCE]

    async def handle_resource_loss(self, resource_name: ResourceIdentifier):
        if self.active:
            await self.scheduler.resources.request_resource(TEST_RESOURCE, priority=1, applicant=self)
            await super().handle_resource_loss(resource_name)

    async def handle_acquired_resource(self, resource_name: ResourceIdentifier):
        if self.active and self.num_steps > 10:
            self.log.info(f"{self.name} - Set to inactive.")
            self.active = False
        await super().handle_acquired_resource(resource_name)
