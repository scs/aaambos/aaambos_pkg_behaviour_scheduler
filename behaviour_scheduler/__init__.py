"""

This is the documentation of the aaambos package Behaviour Scheduler.
It contains an aaambos module and own run and/or arch configs.

# About the package
It provides a behaviour scheduler that handles the start and execution of defined behaviours.

The behaviours need to be implemented elsewhere and should be passed to the scheduler via the config.

# Background / Literature
In the original scs architecture paper (Self-explaining social robots: An explainable behavior generation architecture for human-robot interaction) such module is called Behaviour Controller.

# Usage / Examples
The Scheduler should be used without adaptation. Just mention the necessary behaviours in your config.

# Citation
...

"""
