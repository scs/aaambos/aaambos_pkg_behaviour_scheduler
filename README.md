# Behaviour Scheduler

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_behaviour_scheduler)

A behavior scheduler module and extensions that can extend new behaviours.

## Installation

```bash
pip install behaviour_scheduler@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_behaviour_scheduler.git
```
